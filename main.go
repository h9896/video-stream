package main

import (
	"video-stream/signal"

	"github.com/pion/webrtc/v2"
)

func main() {
	url := []string{"stun:stun.l.google.com:19302"}
	media := signal.NewWebrtcAPI()
	ice := []webrtc.ICEServer{signal.NewICEServer(signal.IceURL(url))}
	config := signal.NewPeerConnectionConfig(signal.RtcICE(ice))
	signal.NewSignalServer("127.0.0.1", "8080", media, config)
}
