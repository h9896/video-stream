module video-stream

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/pion/rtcp v1.2.3
	github.com/pion/webrtc/v2 v2.2.26
)
