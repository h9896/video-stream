package signal

import (
	"io"
	"log"
	"sync"
	"time"

	"github.com/pion/rtcp"
	"github.com/pion/webrtc/v2"
)

const rtcpPLIInterval = time.Second * 3

var connectionMap peerConnection = peerConnection{item: make(map[string]chan *webrtc.Track)}

func NewWebrtcAPI() *webrtc.API {
	media := webrtc.MediaEngine{}
	media.RegisterCodec(webrtc.NewRTPVP8Codec(webrtc.DefaultPayloadTypeVP8, 90000))
	return webrtc.NewAPI(webrtc.WithMediaEngine(media))
}

func NewPeerConnectionConfig(opts ...rtcOption) webrtc.Configuration {
	config := webrtc.Configuration{}
	for _, val := range opts {
		val(&config)
	}
	return config
}
func NewICEServer(opts ...iceOption) webrtc.ICEServer {
	sever := webrtc.ICEServer{}
	for _, val := range opts {
		val(&sever)
	}
	return sever
}

func receiveTrack(connection *webrtc.PeerConnection, peerID string) {
	defer connectionMap.rwm.Unlock()
	connectionMap.rwm.Lock()
	if _, ok := connectionMap.item[peerID]; !ok {
		connectionMap.item[peerID] = make(chan *webrtc.Track, 1)
	}
	localTrack := <-connectionMap.item[peerID]
	connection.AddTrack(localTrack)
}

func createTrack(connection *webrtc.PeerConnection, currentUserID string) {
	if _, err := connection.AddTransceiver(webrtc.RTPCodecTypeVideo); err != nil {
		log.Fatal(err)
	}

	// Set a handler for when a new remote track starts, this just distributes all our packets
	// to connected peers
	connection.OnTrack(func(remoteTrack *webrtc.Track, receiver *webrtc.RTPReceiver) {
		// Send a PLI on an interval so that the publisher is pushing a keyframe every rtcpPLIInterval
		// This can be less wasteful by processing incoming RTCP events, then we would emit a NACK/PLI when a viewer requests it
		go func() {
			ticker := time.NewTicker(rtcpPLIInterval)
			for range ticker.C {
				if rtcpSendErr := connection.WriteRTCP([]rtcp.Packet{&rtcp.PictureLossIndication{MediaSSRC: remoteTrack.SSRC()}}); rtcpSendErr != nil {
					log.Println(rtcpSendErr)
				}
			}
		}()

		// Create a local track, all our SFU clients will be fed via this track
		// main track of the broadcaster
		localTrack, newTrackErr := connection.NewTrack(remoteTrack.PayloadType(), remoteTrack.SSRC(), "video", "pion")
		if newTrackErr != nil {
			log.Fatal(newTrackErr)
		}
		// the channel that will have the local track that is used by the sender
		// the localTrack needs to be fed to the reciever
		connectionMap.rwm.Lock()
		if existingChan, ok := connectionMap.item[currentUserID]; ok {
			// feed the exsiting track from user with this track
			existingChan <- localTrack
		} else {
			connectionMap.item[currentUserID] = make(chan *webrtc.Track, 1)
			connectionMap.item[currentUserID] <- localTrack
		}
		connectionMap.rwm.Unlock()

		rtpBuf := make([]byte, 1400)
		for { // for publisher only
			i, readErr := remoteTrack.Read(rtpBuf)
			if readErr != nil {
				log.Fatal(readErr)
			}

			// ErrClosedPipe means we don't have any subscribers, this is ok if no peers have connected yet
			if _, err := localTrack.Write(rtpBuf[:i]); err != nil && err != io.ErrClosedPipe {
				log.Fatal(err)
			}
		}
	})

}

func RtcICE(ice []webrtc.ICEServer) rtcOption {
	return func(rtc *webrtc.Configuration) {
		rtc.ICEServers = ice
	}
}
func IceURL(val []string) iceOption {
	return func(config *webrtc.ICEServer) {
		config.URLs = val
	}
}

type rtcOption func(*webrtc.Configuration)
type iceOption func(*webrtc.ICEServer)
type peerConnection struct {
	item map[string]chan *webrtc.Track
	rwm  sync.RWMutex
}
type sdp struct {
	sdp string
}
