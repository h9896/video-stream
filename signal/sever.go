package signal

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/pion/webrtc/v2"
)

func NewSignalServer(address, port string, api *webrtc.API, config webrtc.Configuration) {
	router := gin.Default()
	router.POST("/webrtc/sdp/m/:meetingId/c/:userID/p/:peerId/s/:isSender", handlerPOST(api, config))
	router.Run(fmt.Sprintf("%v:%v", address, port))
}

func handlerPOST(api *webrtc.API, config webrtc.Configuration) gin.HandlerFunc {
	return func(c *gin.Context) {
		isSender, _ := strconv.ParseBool(c.Param("isSender"))
		userID := c.Param("user")
		peerID := c.Param("peerID")
		var session sdp
		if err := c.ShouldBindJSON(&session); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		offer := webrtc.SessionDescription{}
		Decode(session.sdp, &offer)
		peerConnection, err := api.NewPeerConnection(config)
		if err != nil {
			log.Fatal(err)
		}
		if !isSender {
			receiveTrack(peerConnection, peerID)
		} else {
			createTrack(peerConnection, userID)
		}
		// Set the SessionDescription of remote peer
		peerConnection.SetRemoteDescription(offer)

		// Create answer
		answer, err := peerConnection.CreateAnswer(nil)
		if err != nil {
			log.Fatal(err)
		}
		// Sets the LocalDescription, and starts our UDP listeners
		err = peerConnection.SetLocalDescription(answer)
		if err != nil {
			log.Fatal(err)
		}
		c.JSON(http.StatusOK, sdp{sdp: Encode(answer)})
	}
}
